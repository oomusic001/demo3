package com.gupaoedu.vip.admin.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ConversionServiceFactoryBean;
import org.springframework.stereotype.Component;

@Component
public class ConversionServiceFactoryBeanConfig {

    @Bean
    public ConversionServiceFactoryBean conversionServiceFactoryBean(){
        return new ConversionServiceFactoryBean();
    }
}
