package com.gupaoedu.vip.admin.properties;

import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
@Component
public class MybatisConfig {
    public Map<String ,String> getMybatisConfig(){
        InputStream resourceAsStream = this.getClass().getResourceAsStream("/properties/mybatis.properties");
        Properties properties = new Properties();
        Map<String ,String> config = null;
        try {
            properties.load(resourceAsStream);
            config = new HashMap<>();
            config.put("username",properties.getProperty("username"));
            config.put("password",properties.getProperty("password"));
            config.put("driverClassName",properties.getProperty("driverClassName"));
            config.put("url",properties.getProperty("url"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return config;
    }
}
