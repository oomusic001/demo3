package com.gupaoedu.vip.admin.dao.mapper;

import com.gupaoedu.vip.admin.entity.po.User;
import org.apache.ibatis.annotations.Mapper;
import org.mybatis.spring.annotation.MapperScan;

import java.util.List;
public interface UserMapper {
    public List<User> selectUser();
    public void insertUser(User user);
    public void updateById(User user);
    public  void deleteById(Integer userId);
}
