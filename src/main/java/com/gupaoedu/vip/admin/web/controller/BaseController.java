package com.gupaoedu.vip.admin.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;


public abstract  class BaseController {
    @Autowired
    private Validator validator;

    /**
     * 前端访问controller时，进行数据绑定。
     * @param webDataBinder
     */
    @InitBinder
    protected void initBinder(WebDataBinder webDataBinder){
        webDataBinder.addValidators(validator);


    }

}
