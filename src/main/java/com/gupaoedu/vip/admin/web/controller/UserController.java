package com.gupaoedu.vip.admin.web.controller;

import com.gupaoedu.vip.admin.annotation.RestMessage;
import com.gupaoedu.vip.admin.entity.po.User;
import com.gupaoedu.vip.admin.properties.MybatisConfig;
import com.gupaoedu.vip.admin.service.UserService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;

@Controller
public class UserController extends BaseController{
    private static final Logger logger =
            LoggerFactory.getLogger(UserController.class);
    @Autowired
    private UserService userService;
    @Autowired
    private MybatisConfig mybatisConfig;
    @RequestMapping("getUser")
    @RestMessage
    @ResponseBody
    public List<User> getUser(){
        List<User> list = userService.getUserList();
        logger.info("查询所有用户结果："+list);
        return list;
    }

    @RequestMapping("addUser")
    public String addUser(User user){
        userService.addUser(user);
        return "index";
    }

    @RequestMapping("updateById")
    public String updateById(User user){
        userService.updateById(user);
        return "index";
    }
    @RequestMapping("deleteById")
    public String deleteById(Integer userId){
        userService.deleteById(userId);
        return "index";
    }
    @RequestMapping("getMybatisConfig")
    @ResponseBody
    public Map<String ,String> getMybatisConfig(){
        return mybatisConfig.getMybatisConfig();
    }
}
