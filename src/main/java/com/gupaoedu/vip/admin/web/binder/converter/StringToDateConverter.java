package com.gupaoedu.vip.admin.web.binder.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ConversionServiceFactoryBean;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;

/**
 * 自定义类型转换器
 *      当前端输入的数据格式（字符串）后台无法转化为java格式时使用
 *      如：yyyy--MM--dd -->  2012-02-03
 */
@Component
public class StringToDateConverter implements Converter<String ,Date>{
    @Override
    public Date convert(String s) {
        try {
            return new SimpleDateFormat("yyyy--MM--dd").parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
    @Autowired
    private ConversionServiceFactoryBean conversionBean;

    /**
     * 初始化注册自定义的类型转换器
     */
    @PostConstruct
    public void init(){
        conversionBean.setConverters(Collections.singleton(new HashSet<>().add(new StringToDateConverter())));
    }
}
