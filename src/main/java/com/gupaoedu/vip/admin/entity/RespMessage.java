package com.gupaoedu.vip.admin.entity;

public class RespMessage {
    public Object data;
    public Integer code;
    public String message;
    public long timeStamp;
    public boolean success;

    public Object getData() {
        return data;
    }

    public RespMessage setData(Object data) {
        this.data = data;
        return this;
    }

    public Integer getCode() {
        return code;
    }

    public RespMessage setCode(Integer code) {
        this.code = code;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public RespMessage setMessage(String message) {
        this.message = message;
        return this;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public RespMessage setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
        return this;
    }

    public boolean isSuccess() {
        return success;
    }

    public RespMessage setSuccess(boolean success) {
        this.success = success;
        return this;
    }
    public static RespMessage ok(){
        return new RespMessage();
    }
}
