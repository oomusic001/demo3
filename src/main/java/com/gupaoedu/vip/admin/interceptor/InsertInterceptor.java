package com.gupaoedu.vip.admin.interceptor;

import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.*;

import java.io.Serializable;
import java.util.Properties;
@Intercepts({
        @Signature(
                type = Executor.class,
                method = "update",
                args = {MappedStatement.class,Object.class}
        )
})
public class InsertInterceptor implements Interceptor, Serializable {

    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        System.out.println("拦截了update or insert or delete 方法");
        return invocation.proceed();
    }

    @Override
    public Object plugin(Object o) {
        return Plugin.wrap(o,this);
    }

    @Override
    public void setProperties(Properties properties) {

    }
}
