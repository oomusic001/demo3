package com.gupaoedu.vip.admin.interceptor;

import com.gupaoedu.vip.admin.entity.po.User;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.*;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;

import java.io.Serializable;
import java.util.Properties;
@Intercepts({
        @Signature(
                type = Executor.class,
                method = "query",
                args = {MappedStatement.class,Object.class, RowBounds.class, ResultHandler.class}
        )
})
public class PagerInterceptor implements Interceptor , Serializable {
    /**
     *      * 解释这个方法前，我们一定要理解方法参数 {@link Invocation} 是个什么鬼？
     *      * 1 我们知道，mybatis拦截器默认只能拦截四种类型 Executor、StatementHandler、ParameterHandler和 ResultSetHandler
     *      * 2 不管是哪种代理，代理的目标对象就是我们要拦截对象，举例说明：
     *      *      比如我们要拦截 {@link Executor#update(MappedStatement ms, Object parameter)} 方法，
     *      *      那么 Invocation 就是这个对象，Invocation 里面有三个参数 target method args
     *      *          target 就是 Executor
     *      *          method 就是 update
     *      *          args   就是 MappedStatement ms, Object parameter
     *      *  该方法在运行时调用 原文链接：https://blog.csdn.net/Liu_York/article/details/88053053
     * @param invocation
     * @return
     * @throws Throwable
     *
     * https://www.jianshu.com/p/6fb36263f032
     */
    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        System.out.println("拦截了query方法");
        MappedStatement ms = (MappedStatement) invocation.getArgs()[0];
        Object parameter = invocation.getArgs()[1];
        BoundSql boundSql = ms.getBoundSql(parameter);
        //获取执行sql语句的参数（对象）
        Object object = boundSql.getParameterObject();
        //todo
        if (object instanceof User){
            User user  = (User) object;
            //从user中获取分页参数    user.getPager();
        }
        Executor executor = (Executor) invocation.getTarget();
        String sql = boundSql.getSql();
        //执行开始时间
        Long startTime = System.currentTimeMillis();
        //获取执行结果
        Object result = invocation.proceed();
        //执行结束时间
        Long endTime = System.currentTimeMillis();
        //耗时
        System.out.println("执行sql："+sql+" 耗时："+(endTime-startTime));

        return result;
    }

    /**
     * 调用插件
     * Plugin的wrap方法，它根据当前的Interceptor上面的注解定义哪些接口需要拦截，
     * 然后判断当前目标对象是否有实现对应需要拦截的接口，如果没有则返回目标对象本身，
     * 如果有则返回一个代理对象。而这个代理对象的InvocationHandler正是一个Plugin。
     * 所以当目标对象在执行接口方法时，如果是通过代理对象执行的，则会调用对应InvocationHandler的invoke方法，
     * 也就是Plugin的invoke方法。所以接着我们来看一下该invoke方法的内容。这里invoke方法的逻辑是：
     * 如果当前执行的方法是定义好的需要拦截的方法，则把目标对象、要执行的方法以及方法参数封装成一个Invocation对象，
     * 再把封装好的Invocation作为参数传递给当前拦截器的intercept方法。如果不需要拦截，则直接调用当前的方法。
     * Invocation中定义了定义了一个proceed方法，其逻辑就是调用当前方法，所以如果在intercept中需要继续调用
     * 当前方法的话可以调用invocation的procced方法。
     * 原文链接：https://blog.csdn.net/Liu_York/article/details/88053053
     * @param o
     * @return
     */
    @Override
    public Object plugin(Object o) {
        //Executor , ResultSetHandler,StatementHandler,ParameterHandler,
        //这是Mybatis中的四大对象，也是拦截器的切入点
        //https://blog.csdn.net/niunai112/article/details/80484635
        System.out.println("mybatis调用插件:"+o.toString());
        //把拦截器对象封装成Plugin代理对象.
        return Plugin.wrap(o,this);
    }
    /**
     * 这个方法最好理解，如果我们拦截器需要用到一些变量参数，而且这个参数是支持可配置的，
     *  类似Spring中的@Value("${}")从application.properties文件获取
     * 这个时候我们就可以使用这个方法
     *
     * 如何使用？
     * 只需要在 mybatis 配置文件中加入类似如下配置，然后 {@link Interceptor#setProperties(Properties)} 就可以获取参数
     *      <plugin interceptor="liu.york.mybatis.study.plugin.MyInterceptor">
     *           <property name="username" value="LiuYork"/>
     *           <property name="password" value="123456"/>
     *      </plugin>
     *      方法中获取参数：properties.getProperty("username");
     *
     * 问题：为什么要存在这个方法呢，比如直接使用 @Value("${}") 获取不就得了？
     * 原因是 mybatis 框架本身就是一个可以独立使用的框架，没有像 Spring 这种做了很多依赖注入的功能
     *
     *  该方法在 mybatis 加载核心配置文件时被调用
     *  原文链接：https://blog.csdn.net/Liu_York/article/details/88053053
     */

    @Override
    public void setProperties(Properties properties) {


    }
}
