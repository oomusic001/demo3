package com.gupaoedu.vip.admin.logging.sesitive;

import ch.qos.logback.classic.pattern.MessageConverter;
import ch.qos.logback.classic.spi.ILoggingEvent;
import org.springframework.stereotype.Component;

/**
 * 日志拦截器
 */
@Component
public class LoggingConverter extends MessageConverter{

    public LoggingConverter() {
        super();
    }

    /**
     * 拦截后在这里添加脱敏逻辑。
     * @param event
     * @return
     */
    @Override
    public String convert(ILoggingEvent event) {
        return "拦截到的日志："+event.getMessage();
    }
}
