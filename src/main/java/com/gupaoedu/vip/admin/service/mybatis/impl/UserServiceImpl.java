package com.gupaoedu.vip.admin.service.mybatis.impl;

import com.gupaoedu.vip.admin.controller.UserController;
import com.gupaoedu.vip.admin.dao.mapper.UserMapper;
import com.gupaoedu.vip.admin.entity.po.User;
import com.gupaoedu.vip.admin.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class UserServiceImpl implements UserService {
    private static final Logger logger =
            LoggerFactory.getLogger(UserServiceImpl.class);
    @Autowired
    private UserMapper userMapper;
    @Override
    public List<User> getUserList() {
        return userMapper.selectUser();
    }

    @Override
    public void addUser(User user) {
        userMapper.insertUser(user);
        logger.info("添加用户成功(user):"+user.toString());
    }

    @Override
    public void updateById(User user) {
        userMapper.updateById(user);
        logger.info("更改用户成功(user):"+user.toString());
    }

    @Override
    public void deleteById(Integer userId) {
        userMapper.deleteById(userId);
        logger.info("删除用户成功(userId):"+userId);
    }
}
