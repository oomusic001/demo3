package com.gupaoedu.vip.admin.service;


import com.gupaoedu.vip.admin.entity.po.User;

import java.util.List;

public interface UserService {
    public List<User> getUserList();
    public void addUser(User user);
    public void updateById(User user);
    public void deleteById(Integer userId);
}
